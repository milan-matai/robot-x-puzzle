const Puzzle = require('../../models/Puzzle.js');

// ========== HELPERS ========== //

// Used in Solution handler:
async function findDocumentAndSaveData (id, data) {
  let savedData = data || '';
  const document = await Puzzle.findOne({ ID: id });
  if (document) {
    const foundDocument = document.toObject({ getters: true });
    savedData += foundDocument.data;
    return findDocumentAndSaveData(foundDocument.nextID, savedData);
  } else {
    return savedData;
  }
}

// ========== HANDLERS ========== //

module.exports.getAll = async (req, res) => {
  try {
    const allPuzzle = await Puzzle.find({});
    res.status(200).send(allPuzzle);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports.findzero = async (req, res) => {
  try {
    let firstPuzzle = await Puzzle.find({ ID: 0 });
    res.status(200).send(firstPuzzle);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports.solution = async (req, res) => {
  try {
    let startID = 0;
    let result = await findDocumentAndSaveData(startID);
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
