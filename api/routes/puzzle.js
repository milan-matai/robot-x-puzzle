const { Router } = require('express');
const handlers = require('./handlers/puzzle.js');

const router = Router();

router.get('/', handlers.getAll);
router.get('/findzero', handlers.findzero);
router.get('/solution', handlers.solution);

module.exports = router;
