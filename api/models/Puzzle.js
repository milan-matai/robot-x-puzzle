const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mirrorPuzzleSchema = new Schema({
  strict: false
}, {
  collection: 'puzzle'
});

const MirrorPuzzle = mongoose.model('MirrorPuzzle', mirrorPuzzleSchema);
module.exports = MirrorPuzzle;
