const express = require('express');
const app = express();
const mongoose = require('mongoose');

const DATABASE_URL = 'mongodb://testuser:mongotestpass@omen.robot-x.hu:30000/testdatabase';

mongoose.connect(DATABASE_URL);
mongoose.connection.on('open', () => console.log('Connected to DB'));

const puzzleRoutes = require('./routes/puzzle');

app.use('/puzzle', puzzleRoutes);

module.exports = {
  path: '/api',
  handler: app
}
